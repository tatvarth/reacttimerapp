var React = require('react');
var ReactDOM = require('react-dom');
var expect = require('expect');

var $ = require('jQuery');
var TestUtils = require('react-addons-test-utils');
var Clock = require('Clock');


describe('Clock', () => {
  it('should exist', () => {
    expect(Clock).toExist();
  });
});
describe('render', () => {
  it('should render clock to output', () => {
    var clock = TestUtils.renderIntoDocument(<Clock totalSeconds = {62}/>);
    var $el = $(ReactDOM.findDOMNode(clock));
    var actualText = $el.find('.clock-text').text();
    expect(actualText).toBe('01:02');
  });
});
describe('Clock', () => {
  it('should format Seconds', () => {
    var clock = TestUtils.renderIntoDocument(<Clock/>);
    var seconds = 615;
    var expected = '10:15';
    var actual = clock.formatSeconds(seconds);
    expect(actual).toBe(expected);
  });
  it('should format Seconds < 10', () => {
    var clock = TestUtils.renderIntoDocument(<Clock/>);
    var seconds = 6;
    var expected = '00:06';
    var actual = clock.formatSeconds(seconds);
    expect(actual).toBe(expected);
  });
  it('should format Seconds == 60', () => {
    var clock = TestUtils.renderIntoDocument(<Clock/>);
    var seconds = 60;
    var expected = '01:00';
    var actual = clock.formatSeconds(seconds);
    expect(actual).toBe(expected);
  });
  it('should format Seconds == 61', () => {
    var clock = TestUtils.renderIntoDocument(<Clock/>);
    var seconds = 61;
    var expected = '01:01';
    var actual = clock.formatSeconds(seconds);
    expect(actual).toBe(expected);
  });
  it('should format minutes == 10', () => {
    var clock = TestUtils.renderIntoDocument(<Clock/>);
    var seconds = 61;
    var expected = '01:01';
    var actual = clock.formatSeconds(seconds);
    expect(actual).toBe(expected);
  });
});
