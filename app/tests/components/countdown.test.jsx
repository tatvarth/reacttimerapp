var React = require('react');
var ReactDOM = require('react-dom');
var expect = require('expect');
var $ = require('jQuery');
var TestUtils = require('react-addons-test-utils');

var Countdown = require('Countdown');


describe('Countdown', () => {
  it('should exist', () => {
    expect(Countdown).toExist();
  });
  it('should call handleSetCountdown if valid seconds entered', () => {
    var countdown = TestUtils.renderIntoDocument(<Countdown/>);
    countdown.handleSetCountdown(123);
    expect(countdown.state.count).toBe(123);
    expect(countdown.state.countdownStatus).toBe('started');
  });
});
