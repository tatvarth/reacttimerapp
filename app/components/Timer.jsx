var React = require('react');
var Clock = require('Clock');
var Controls = require('Controls');


var Timer = React.createClass({
  getInitialState: function(){
    return {
      count: 0,
      countdownStatus: 'paused'
    };
  },
  handleStatusChange: function(newStatus) {
    this.setState({countdownStatus: newStatus});
  },
  componentDidUpdate: function(prevProps, prevState) {
    if(this.state.countdownStatus !== prevState.countdownStatus) {
      switch(this.state.countdownStatus) {
        case 'started':
          this.startTimer();
          break;
        case 'stopped':
          this.setState({
            count: 0,
            countdownStatus: 'paused'
          });
        case 'paused':
          clearInterval(this.timer);
          this.timer = undefined;
          break;
      }
    }
  },
  startTimer: function(){
    this.timer = setInterval(() => {
      var newCount = this.state.count + 1;
      this.setState({
        count: newCount >= 0 ? newCount : 0
      });
      if(newCount == 0){
        clearInterval(this.timer);
        this.setState({
          countdownStatus: 'stopped'
        });
      }
    },1000);
  },
  handleSetCountdown: function(seconds) {
    this.setState({
      count: seconds,
      countdownStatus: 'started'
    });
  },
  render: function() {
    var {count} = this.state;
    return (
    <div>
      <h1 className="page-title">Timer</h1>
      <Clock totalSeconds={count}/>
      <Controls countdownStatus={this.state.countdownStatus} onStatusChange = {this.handleStatusChange}/>
    </div>
  );
}
});


module.exports = Timer;
